import React from 'react'
import PropTypes from 'prop-types';

const Panel = ({
  activeBooking,
  onClick,
  onChange
}) => {
	return (
		<div className="bookings__panel">
  		<h1>Booking update</h1>
  		<div className="panel">
  			<div
          className="panel__close"
          onClick={onClick}
        />
  			<div className="panel__item">
  				<h2>Name</h2>
  				<p>{activeBooking.title} {activeBooking.firstName} {activeBooking.lastName}</p>
  			</div>
  			<div className="panel__item">
  				<h2>Time</h2>
  				<p>{activeBooking.time}</p>
  			</div>
  			<div className="panel__item">
  				<h2>Covers</h2>
  				<p>{activeBooking.partySize}</p>
  			</div>
  		  <div className="panel__item">
  				<h2>Seated</h2>
  				{
  					!activeBooking.cancelled && (
  						<div>
  							<label>
  	  						<input
  	  							type="radio"
  	  							name="isSeated"
  	  							checked={activeBooking.seated == true}
  	  							onChange={onChange}
  	  							value={true}
                  />
                  Yes
  	  					</label>
  	  					<label>
  	  				  	<input
  	  				  		type="radio"
  	  				  		name="isSeated"
  	  				  		checked={activeBooking.seated == false}
  	  				  		onChange={onChange}
  	  				  		value={false}
                  />
                  No
  	  				  </label>
    				  </div>
            )
  				}

  				{
  					!activeBooking.seated && (
              <label>
                <input
                  type="radio"
                  name="isCancelled"
                  checked={activeBooking.cancelled == true}
                  onChange={onChange}
                  value={false}
                />
                Cancelled
              </label>
  					)
  				}

  			</div>
  			<div className="panel__item">
  				<h2>Notes</h2>
  				{
  					activeBooking.notes ? (
              <p>{activeBooking.notes}</p>
  				  ) : (
              <p>{'No Notes'}</p>
            )
  				}
  			</div>
  		</div>
		</div>
	);
}

Panel.propTypes = {
  activeBooking: PropTypes.shape(),
  onClick: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired
};

Panel.defaultProps = {
  activeBooking: {}
};


export default Panel;
