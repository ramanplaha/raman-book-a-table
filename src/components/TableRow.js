import React from 'react'
import classnames from 'classnames';
import PropTypes from 'prop-types';

/**
 * TableRow
 * @param  {Function} options.onClick
 * @param  {Object} options.booking
 * @return {JSX}
 */
const TableRow = ({
  onClick,
  booking,
  isActive
}) => {
	return (
		<tr
      className={
        classnames(
          {
            'booking__cancelled': booking.cancelled
          },
          {
            'booking__row--active': isActive
          }
        )
      }
      onClick={onClick}
    >
		 	<td
        colSpan="3"
      >
        <p>{booking.title} {booking.firstName} {booking.lastName}</p>
      </td>
		 	<td>
        <p>{booking.time}</p>
      </td>
		 	<td>
        <p>{booking.partySize}</p>
      </td>
		 	<td>
			 	{
			 		booking.seated ? (
						<p>{'Y'}</p>
				  ) :
				  	<p>{'N'}</p>
			 	}
		 	</td>
	 	</tr>
	);
}

TableRow.propTypes = {
  booking: PropTypes.shape(),
  onClick: PropTypes.func.isRequired,
  isActive: PropTypes.bool.isRequired
};

export default TableRow;
