import React, { Component } from 'react';

import TableRow from '../components/TableRow';
import Panel from '../components/Panel';


class Bookings extends Component {
  /**
   * constructor
   */
	constructor() {
		super();

		this.state = {
			data: [],
      showPanel: false,
      activeBookingInfo: {
        index: null
      }
		}

    this.handleChange = this.handleChange.bind(this);
	}

	/**
	 * Use componentDidMount lifecycle hook to execute
	 * our fetch method that retrieves JSON date from
	 * our endpoint
	 * @return [Array]
	 */
	componentDidMount() {
		fetch('data/bookings.json')
			.then(res => res.json())
			.then(data => this.setState({
				data: data[0]
			}));
	}

	/**
	 * Use the index passed to the function to get
	 * the active booking in the existing data.bookings state
	 * @param {Integer}
	 */
	getBookingInfo(index) {
		const activeBookingInfo = this.state.data.bookings[index];

		this.setState({
      activeBookingInfo: Object.assign(
        activeBookingInfo, {
          index
        }
      )
    });

    if (!this.state.showPanel) {
		  this.togglePanel();
    }
	}

  /**
   * handleChange
   * Update state based on booking status
   * @param  {Object} 
   */
	handleChange(event) {
		if (event.target.name === 'isSeated') {
      const seated = event.target.value === 'true';

			this.setState({
        activeBookingInfo: Object.assign(
          this.state.activeBookingInfo, {
            seated
          }
        )
      })

		} else if (event.target.name === 'isCancelled') {
      const cancelled = event.target.value === 'false';

      this.setState({
        activeBookingInfo: Object.assign(
          this.state.activeBookingInfo, {
            cancelled
          }
        )
      })
		}
	}

  /**
   * togglePanel
   * Toggle the details panel
   */
	togglePanel() {
    const isPanelActive = this.state.showPanel;

    if (isPanelActive) {
      this.setState({
        showPanel: false
      })
    } else {
      this.setState({
        showPanel: true
      })
    }
	}

  formatDate(inputDate) {
    var date = new Date(inputDate);
    if (!isNaN(date.getTime())) {
        // Months use 0 index.
        return date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
    }
  }

  /**
   * render
   * @return {JSX}
   */
	render() {
		return (
			<div className="bookings">
				{
					this.state.data.bookings && (
						<div className="bookings__table">
							<h1>
                Booking for {this.formatDate(this.state.data.date)}
              </h1>
							<table>
  						  <thead>
  						    <tr>
  						      <th colSpan="3">Name</th>
  						      <th>Time</th>
  						      <th>Covers</th>
  						      <th>Seated</th>
  						    </tr>
  						  </thead>
                <tbody>
    							{
    								this.state.data.bookings.map((booking, index) =>
    									<TableRow
                        key={index}
                        booking={booking}
                        isActive={index === this.state.activeBookingInfo.index}
                        onClick={() => this.getBookingInfo(index)}
                      />
    								)
    							}
                </tbody>
							</table>
						</div>
          )
				}
				{
					this.state.activeBookingInfo
            && this.state.showPanel &&
  						<Panel
                activeBooking={this.state.activeBookingInfo}
                onClick={() => this.togglePanel()}
                onChange={this.handleChange}
              />
				}
			</div>
		)
	}
}

export default Bookings;
