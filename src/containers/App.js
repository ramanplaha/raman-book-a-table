import React, { Component } from 'react';

import Header from '../components/Header';
import Bookings from './Bookings';
import '../styles/App.scss';

export default class App extends Component {
  render() {
    return (
    	<div>
	      <Header />
		    <Bookings />
      </div>
    );
  }
}
