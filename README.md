# Raman


# Installation and Usage

Hi, 

To start the app open a CLI and `cd raman-book-a-table`.
Run `npm install` to install any packages this app depends on. 

Run `npm start` to run a local development server. Open a browser of your choice, load `http://localhost:3000` (This can be configured in `webpack.config.js`) and enjoy. 

Run `npm run build` to build for production. To view the built version open the CLI, `cd dist` and then run `python -m SimpleHTTPServer 7777`.
Open a browser and load `http://localhost:7777` to view the app in production.

_NB: if the python command doesn't work you will need to install python or put the contents of the dist folder onto a server._

The app is available on `http://react-booking-app.s3-website.eu-west-2.amazonaws.com/`


# Feedback

I really enjoyed spending time developing this app. I thought about how I would handle the state. I figured I could house all the state in a stateful component. Then send the state down to the stateless child components.

I felt this was a good app to build to test my JS and React knowledge. I am happy with my attempt and feel I have demonstrated a good understanding of JavaScript ES6 and React. 
I did not include unit tests for this app, the reason for this was I wanted to focus more on refactoring my code and making sure I have met the acceptance criteria. 

I made some improvements to the app by adding an active state to the booking that was being viewed. Having an active state would allow the restaurant concierge to clearly tell which booking they were looking at. 
I added some more logic that said; if the guest was marked as seated don't show the cancelled radio button anymore because it doesn't make sense too.
I did something similar with the cancel button; If the guest has cancelled then don't show the Yes or No radio buttons. This was done because once cancelled you you don't want the restaurant concierge to still be able to mark them as seated. 
I made a subtle UI change which was to add a red line through the cancelled booking to make it a bit more clear to the restaurant concierge.

You could make another improvement by storing the state in the browsers localStorage. Currently when you refresh the page you lose any changes made. Implementing local storage would cache the state. 
If the application grew I would maybe start thinking about using Redux to handle the entire applications state.

# Basic React dev environment with hot loading

To be used as part of the _Bookatable Front End and React Assessment_.  
**See the included `task_docs` folder for full instructions and wireframes.**

### Requirements

Built using


```
Node.js:   8.12.0  
npm:       6.4.1
```


### Usage

Hot loading / live-editing React components is enabled.

```
npm install
npm start
open http://localhost:3000
```


#### WebStorm Users

Because the WebStorm IDE uses "safe writes" by default, Webpack's file-watcher won't recognize file changes, so hot-loading won't work. To fix this, disable "safe write" in WebStorm.


### Building

A basic production script is included that builds your app to a `dist` folder

```
npm run build
```


### Dependencies

* React
* Webpack
* [webpack-dev-server](https://github.com/webpack/webpack-dev-server)
* [babel-loader](https://github.com/babel/babel-loader)
* [react-hot-loader](https://github.com/gaearon/react-hot-loader)

---

